#include "aecl.hpp"

SAS_Plan AECL::optimize(SAS_Plan plan, ExperimentResult& result) {
    size_t plan_size = plan.get_length();
    auto landmark_search_start = std::chrono::high_resolution_clock::now();
    std::vector<bool> is_landmark = find_landmarks(plan, true, result);
    auto landmark_search_end = std::chrono::high_resolution_clock::now();
    auto landmark_search_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(landmark_search_end - landmark_search_start);
    result.set_time_landmark_search_consumed(landmark_search_microseconds);
    std::vector<bool> removed(plan_size, false);
    std::vector<size_t> marks;
    SAS_State s = plan.get_problem()->get_init();
    for (size_t i = 0; i < plan_size; i += 1) {
        if (!removed[i]) {
            if (!is_landmark[i]) {
                marks.emplace_back(i);
                SAS_State checking_state = s;
                SAS_Assignment changed_assignments = get_grounded_effects(plan.get_operator(i), checking_state);
                bool cycle_detected = false;
                bool violated = false;
                for (size_t j = i + 1; j < plan_size; j += 1) {
                    if (!removed[j]) {
                        if (!violated) {
                            for (const auto& conditional_effect : plan.get_operator(j).get_effects()) {
                                for (const auto& precondition : conditional_effect.first) {
                                    if (changed_assignments.contains(precondition.first)) {
                                        violated = true;
                                        break;
                                    }
                                }
                                if (violated) {
                                    break;
                                }
                            }
                        }
                        if (!checking_state.is_applicable(plan.get_operator(j))) {
                            if (is_landmark[j]) {
                                result.increase_pals_used();
                                break;
                            } else {
                                marks.push_back(j);
                                if (!violated) {
                                    changed_assignments.apply(get_grounded_effects(plan.get_operator(j), checking_state));
                                    if (checking_state.get_assignment().satisfy_all(changed_assignments)) {
                                        cycle_detected = true;
                                        result.increase_cycles_used();
                                        break;
                                    }
                                }
                            }
                        } else {
                            if (!violated) {
                                SAS_Assignment j_effects = get_grounded_effects(plan.get_operator(j), checking_state);
                                for (const auto& p : j_effects) {
                                    if (changed_assignments.contains(p.first) && !changed_assignments.satisfy(p.first, p.second)) {
                                        violated = true;
                                        break;
                                    }
                                }
                            }
                            checking_state.apply(plan.get_operator(j));
                        }
                    }
                }
                if (cycle_detected || checking_state.is_goal()) {
                    result.increase_sets_detected();
                    result.append_set_lenght(marks.size());
                    result.append_set_span(marks[marks.size() - 1] - marks[0]);
                    for (auto index : marks) {
                        removed[index] = true;
                    }
                } else {
                    s.apply(plan.get_operator(i));
                }
                marks.clear();
            } else {
                result.increase_pals_used();
                s.apply(plan.get_operator(i));
            }
        }
    }
    for (size_t i = 0; i < plan_size; i += 1) {
        if (removed[plan_size - 1 - i]) {
            plan.remove(plan_size - 1 - i);
        }
    }
    return plan;
}