#include "aecfd.hpp"

SAS_Plan AECfd::optimize(SAS_Plan plan, ExperimentResult& result) {
    size_t plan_size = plan.get_length();
    std::vector<bool> removed(plan_size, false);
    std::vector<size_t> marks;
    SAS_State s = plan.get_problem()->get_init();
    for (size_t i = 0; i < plan_size; i += 1) {
        if (!removed[i]) {
            marks.emplace_back(i);
            SAS_State checking_state = s;
            SAS_State original_state = s;
            original_state.apply(plan.get_operator(i));
            bool cycle_detected = false;
            for (size_t j = i + 1; j < plan_size; j += 1) {
                if (!removed[j]) {
                    original_state.apply(plan.get_operator(j));
                    if (!checking_state.check_and_apply(plan.get_operator(j))) {
                        marks.push_back(j);
                    }
                    cycle_detected = original_state == checking_state;
                    if (cycle_detected) {
                        result.increase_cycles_used();
                        break;
                    }
                }
            }
            if (cycle_detected || checking_state.is_goal()) {
                result.increase_sets_detected();
                result.append_set_lenght(marks.size());
                result.append_set_span(marks[marks.size() - 1] - marks[0]);
                for (auto index : marks) {
                    removed[index] = true;
                }
            } else {
                s.apply(plan.get_operator(i));
            }
            marks.clear();
        }
    }
    for (size_t i = 0; i < plan_size; i += 1) {
        if (removed[plan_size - 1 - i]) {
            plan.remove(plan_size - 1 - i);
        }
    }
    return plan;
}