#include "optimizer.hpp"

std::pair<SAS_Plan, std::chrono::duration<long, std::micro>> Optimizer::timed_optimization(SAS_Plan& plan, ExperimentResult& result, bool verbose) {
    
    auto start = std::chrono::high_resolution_clock::now();
    SAS_Plan p = optimize(plan, result);
    auto end = std::chrono::high_resolution_clock::now();
    
    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::pair<SAS_Plan, std::chrono::duration<long, std::micro>> pair = std::make_pair(p, microseconds);

    if (verbose) std::cout << "Optimizing has taken " << microseconds.count() << " microseconds." << std::endl;

    return pair;
}