#include "../include/utils.hpp"

SAS_Assignment get_grounded_effects(const SAS_Operator& o, const SAS_State& state) {
    SAS_Assignment assignment;
    for (const auto& conditional_effect : o.get_effects()) {
        if (state.satisfy_all(conditional_effect.first)) {
            assignment.apply(conditional_effect.second);
        }
    }
    return assignment;
}

std::vector<SAS_State> get_states_along_plan(const SAS_Plan& plan, const std::vector<bool>& removed) {
    size_t size = plan.get_length();
    std::vector<SAS_State> states;
    states.reserve(size);
    SAS_State state = plan.get_problem()->get_init();
    states.push_back(state);
    for (size_t i = 0; i < size; i += 1) {
        if (!removed.at(i)) {
            state.apply(plan.get_operator(i));
        }
        states.push_back(state);
    }
    return states;
}

void update_states_along_plan(std::vector<SAS_State>& states, const SAS_Plan& plan, const std::vector<bool>& removed, size_t start_with_action_index, size_t end_with_action_index) {
    SAS_State state = states.at(start_with_action_index);
    for (size_t i = start_with_action_index; i < end_with_action_index; i += 1) {
        if (!removed.at(i)) {
            state.apply(plan.get_operator(i));
        }
        states.at(i + 1) = state;
    }
}