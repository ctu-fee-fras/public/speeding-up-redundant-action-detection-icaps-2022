#include "ael.hpp"

SAS_Plan AEL::optimize(SAS_Plan plan, ExperimentResult& result) {
    size_t plan_size = plan.get_length();
    auto landmark_search_start = std::chrono::high_resolution_clock::now();
    std::vector<bool> is_landmark = find_landmarks_enhanced(plan, result);
    auto landmark_search_end = std::chrono::high_resolution_clock::now();
    auto landmark_search_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(landmark_search_end - landmark_search_start);
    result.set_time_landmark_search_consumed(landmark_search_microseconds);
    std::vector<bool> removed(plan_size, false);
    std::vector<size_t> marks;
    SAS_State s = plan.get_problem()->get_init();
    for (size_t i = 0; i < plan_size; i += 1) {
        if (!removed[i]) {
            if (!is_landmark[i]) {
                marks.emplace_back(i);
                SAS_State checking_state = s;
                for (size_t j = i + 1; j < plan_size; j += 1) {
                    if (!removed[j]) {
                        if (!checking_state.check_and_apply(plan.get_operator(j))) {
                            if (is_landmark[j]) {
                                result.increase_pals_used();
                                break;
                            } else {
                                marks.push_back(j);
                            }
                        }
                    }
                }
                if (checking_state.is_goal()) {
                    result.increase_sets_detected();
                    result.append_set_lenght(marks.size());
                    result.append_set_span(marks[marks.size() - 1] - marks[0]);
                    for (auto index : marks) {
                        removed[index] = true;
                    }
                } else {
                    s.apply(plan.get_operator(i));
                }
                marks.clear();
            } else {
                result.increase_pals_used();
                s.apply(plan.get_operator(i));
            }
        }
    }
    for (size_t i = 0; i < plan_size; i += 1) {
        if (removed[plan_size - 1 - i]) {
            plan.remove(plan_size - 1 - i);
        }
    }
    return plan;
}