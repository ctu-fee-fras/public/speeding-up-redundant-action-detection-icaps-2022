#include "landmarks.hpp"

std::vector<bool> find_landmarks(const SAS_Plan& plan, bool enhance, ExperimentResult& experiment_result) {

    size_t plan_lenght = plan.get_length();
    unsigned int pals_found_in_iteration = 0;
    
    std::map<std::pair<unsigned int, unsigned int>, std::shared_ptr<std::vector<int>>> achievers;
    for (const auto& pair : plan.get_problem()->get_init().get_assignment()) {
        achievers[pair] = std::shared_ptr<std::vector<int>>(new std::vector<int>(1, -1));
    }
    for (size_t i = 0; i < plan_lenght; i += 1) {
        for (const auto& pair : get_maximal_effects(plan.get_operator(i))) {
            auto result = achievers.emplace(pair, std::shared_ptr<std::vector<int>>(new std::vector<int>(1, i)));
            if (!result.second) {
                result.first->second->push_back(i);
            }
        }
    }

    std::vector<bool> landmarks(plan.get_length(), false);
    std::vector<std::pair<unsigned int, unsigned int>> assignments(plan.get_problem()->get_goal().get_assignment().begin(), plan.get_problem()->get_goal().get_assignment().end());
    for (const auto& pair : assignments) {
        auto it = achievers.find(pair);
        if (it->second->size() == 1 && it->second->front() != -1) {
            if (!landmarks[it->second->front()]) {
                landmarks[it->second->front()] = true;
                experiment_result.increase_pals_found();
                pals_found_in_iteration += 1;
            }
        }
    }
    for (size_t i = 0; i < plan_lenght; i += 1) {
        if (enhance) {
            std::set<std::pair<unsigned int, unsigned int>> maximal_effects = get_maximal_effects(plan.get_operator(plan_lenght - 1 - i));
            for (const auto& certain_assignment : maximal_effects) {
                achievers[certain_assignment]->pop_back();
            }
        }
        if (landmarks[plan_lenght - 1 - i]) {
            std::vector<std::pair<unsigned int, unsigned int>> assignments(plan.get_operator(plan_lenght - 1 - i).get_preconditions().begin(), plan.get_operator(plan_lenght - 1 - i).get_preconditions().end());
            for (const auto& pair : assignments) {
                auto it = achievers.find(pair);
                if (it->second->size() == 1 && it->second->front() != -1) {
                    if (!landmarks[it->second->front()]) {
                        landmarks[it->second->front()] = true;
                        experiment_result.increase_pals_found();
                        pals_found_in_iteration += 1;
                    }
                }
            }
        }
    }
    
    experiment_result.increase_landmarks_sequence_lenght();
    experiment_result.append_landmarks_found(pals_found_in_iteration);
    return landmarks;
}

std::vector<bool> find_landmarks_enhanced(const SAS_Plan& plan, ExperimentResult& experiment_result) {
    return find_landmarks(plan, true, experiment_result);
}

std::set<std::pair<unsigned int, unsigned int>> get_maximal_effects(const SAS_Operator& o) {
    std::set<std::pair<unsigned int, unsigned int>> effects;
    for (const auto& pair : o.get_effects()) {
        for (const auto& assignment_pair : pair.second) {
            effects.insert(assignment_pair);
        }
    }
    return effects;
}

std::set<std::pair<unsigned int, unsigned int>> get_minimal_effects(const SAS_Operator& o) {
    std::set<std::pair<unsigned int, unsigned int>> effects;
    for (auto& pair : o.get_effects()) {
        if (pair.first.empty()) {
            for (auto& assignment_pair : pair.second) {
               effects.insert(assignment_pair);
            }
        }
    }
    return effects;
}