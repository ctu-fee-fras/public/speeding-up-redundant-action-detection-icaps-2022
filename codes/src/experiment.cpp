#include "experiment.hpp"

Experiment::Experiment(std::vector<Benchmark> _benchmarks, std::vector<PlannerProxy> _planners, std::vector<std::reference_wrapper<Optimizer>> _optimizers) : benchmarks(_benchmarks), planners(_planners), optimizers(_optimizers) {}

void Experiment::run(bool verbose) {

    if (!finished) {

        if (verbose) std::cout << "Experiment calculation started." << std::endl;

        if (save_while_running_enabled) {
            if (verbose) std::cout << "Result will be saved into " << save_while_running_filepath << " immediately." << std::endl;
            results.to_csv(save_while_running_filepath, save_while_running_delimiter);
        }

        SAS_Parser parser;
        PlannerProxy translator("translator", FD_TRANSLATOR_SETTING);

        for (Benchmark &benchmark : benchmarks) {

            if (verbose) std::cout << "Starting benchmark " << benchmark.get_name() << "." << std::endl;

            std::filesystem::path sas_problem_path = get_plan_filepath(translator, benchmark);
            std::filesystem::path sas_problem_error_path(sas_problem_path.string() + ".error");
            if (std::filesystem::exists(sas_problem_error_path)) {
                std::cout << "Translation error cached! Benchmark will be skipped." << std::endl;
                continue;
            } else if (!std::filesystem::exists(sas_problem_path)) {
                std::filesystem::create_directories(sas_problem_path.parent_path());
                translator.run(benchmark.get_domain(), benchmark.get_problem(), sas_problem_path.string());
                if (!std::filesystem::exists(sas_problem_path)) {
                    std::cout << "Translated problem missing." << std::endl;
                    std::ofstream error_file(sas_problem_error_path);
                    error_file << "Timeout.";
                    error_file.close();
                }
            } else {
                std::cout << "Using cached translation." << std::endl;
            }
            
            if (std::filesystem::exists(sas_problem_path)) {

                SAS_Problem problem = parser.parse(sas_problem_path);

                for (PlannerProxy &planner : planners) {

                    const std::string plan_filepath = Experiment::get_plan_filepath(planner, benchmark);
                    const std::string time_filepath = plan_filepath + ".timemilliseconds";

                    std::chrono::microseconds planning_time_consumed;

                    try {
                        if (verbose) std::cout << "\t=> running " + planner.get_name() + "... " << std::flush;
                        planning_time_consumed = Experiment::generate_plan(planner, benchmark, plan_filepath, time_filepath);
                    } catch (std::exception& e) {
                        if (verbose) std::cout << "failure! :( Skipping optimization." << std::endl;
                        continue;
                    }

                    if (verbose) std::cout << "success." << std::endl;

                    try {
                        SAS_Plan plan = parser.parse_plan(plan_filepath, problem);
                        if(!plan.is_valid()) {
                            std::cerr << "Generated plan " + plan_filepath + " by " << planner.get_name() << " is not valid! :(" << std::endl;
                            continue;
                        }
                    
                        for (Optimizer &optimizer : optimizers) {

                            ExperimentResult result(planner.get_name(), optimizer.get_name(), benchmark.get_name(), benchmark.get_group(), plan, planning_time_consumed);

                            auto optimizer_results = optimizer.timed_optimization(plan, result);
                            SAS_Plan &optimized_plan = optimizer_results.first;
                            
                            if (!optimized_plan.is_valid()) {
                                std::cerr << "Optimized plan by " << optimizer.get_name() << " is not valid! :(" << std::endl;
                                exit(1);
                            }

                            result.set_optimized_cost(optimizer_results.first.get_cost());
                            result.set_optimized_length(optimizer_results.first.get_length());
                            result.set_time_optimizing_consumed(optimizer_results.second);
                            
                            results.add(result);

                            if (save_while_running_enabled) {
                                result.append_to_csv(save_while_running_filepath, save_while_running_delimiter);
                            }

                        }
                    } catch (std::string& e) {
                        std::cerr << e << std::endl;
                    }
                }
            } else {
                std::cerr << "Translation of " << benchmark.get_name() << " failed. Skiping benchmark." << std::endl;
                continue;
            }
        }

        finished = true;
        std::cout << "Experiments successfully computed." << std::endl;

    } else {
        std::cout << "Experiment already performed. Use Experiment::restart() and Experiment::run() perform it again." << std::endl;
    }
}

void Experiment::display() {
    if (finished) {
        results.dump_all();
    } else {
        std::cout << "Results cannot be dislayed. There are not counted yet." << std::endl;
    }
}

void Experiment::save_while_running(std::string csv_filepath, std::string delimiter) {
    save_while_running_enabled = true;
    save_while_running_filepath = csv_filepath;
    save_while_running_delimiter = delimiter;
}

void Experiment::restart() {
    finished = false;
    save_while_running_enabled = false;
    save_while_running_filepath = "";
    save_while_running_delimiter = "";
    results = ExperimentResults();
}

void Experiment::to_csv(std::string filepath, std::string delimiter) {
    results.to_csv(filepath, delimiter);
}

std::chrono::microseconds Experiment::generate_plan(PlannerProxy& planner, Benchmark& benchmark, const std::filesystem::path plan_filepath, const std::filesystem::path time_filepath) {

    std::filesystem::create_directories(plan_filepath.parent_path());
    std::filesystem::create_directories(time_filepath.parent_path());

    if (!std::filesystem::exists(time_filepath)) {

        auto planning_results = planner.timed_run(benchmark.get_domain(), benchmark.get_problem(), plan_filepath);
        int planning_return_code = planning_results.first;
        auto planning_time_consumed = planning_results.second; 

        if (planning_return_code != 0 || WEXITSTATUS(planning_return_code) != 0) {
            std::filesystem::remove(plan_filepath);
            std::ofstream f(time_filepath);
            f << "infinity\nplanning failed\n"; 
            f.close();
            throw std::exception();
        }

        std::ofstream f(time_filepath);
        f << planning_time_consumed.count() << std::endl;
        f.close();

        return planning_time_consumed;

    } else {
        
        if (!std::filesystem::exists(plan_filepath)) {

            std::cout << "cached "; // prepend before result
            throw std::exception();

        } else {

            long time_consumed;

            std::ifstream f(time_filepath);
            f >> time_consumed;
            f.close();

            std::cout << "cached "; // prepend before result
            return std::chrono::microseconds(time_consumed);

        }     

    }
   
}

std::filesystem::path Experiment::get_plan_filepath(PlannerProxy& planner, Benchmark& benchmark) {
    return std::filesystem::path(std::string(PLANS_LOCATION) + planner.get_name() + "/" + benchmark.get_group() + "/" + benchmark.get_name() + ".solution");
}
