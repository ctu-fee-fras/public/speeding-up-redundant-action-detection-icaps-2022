#include "gael.hpp"

SAS_Plan GAEL::optimize(SAS_Plan plan, ExperimentResult& experiment_result) {
    size_t plan_size = plan.get_length();
    auto landmark_search_start = std::chrono::high_resolution_clock::now();
    std::vector<bool> is_landmark = find_landmarks_enhanced(plan, experiment_result);
    auto landmark_search_end = std::chrono::high_resolution_clock::now();
    auto landmark_search_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(landmark_search_end - landmark_search_start);
    experiment_result.set_time_landmark_search_consumed(landmark_search_microseconds);
    std::vector<bool> removed(plan_size, false);
    int best_index;
    std::pair<int, std::vector<size_t>> best;
    do {
        best_index = -1;
        best = std::make_pair(0, std::vector<size_t>());
        SAS_State s = plan.get_problem()->get_init();
        for (size_t i = 0; i < plan_size; i += 1) {
            if (!removed[i]) {
                if (!is_landmark[i]) {
                    auto result = evaluate_remove(plan, i, s, is_landmark, removed, experiment_result);
                    if (result.first >= best.first) {
                        best_index = (int)i;
                        best = result;
                    }
                } else {
                    experiment_result.increase_pals_used();
                }
                s.apply(plan.get_operator(i));
            }
        }
        if (best_index != -1) {        
            for (auto index : best.second) {
                removed[index] = true;
            }
        }
    } while (best_index != -1);
    for (size_t i = 0; i < plan_size; i += 1) {
        if (removed[plan_size - 1 - i]) {
            plan.remove(plan_size - 1 - i);
        }
    }
    return plan;
}

std::pair<int, std::vector<size_t>> GAEL::evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& s, const std::vector<bool>& is_landmark, const std::vector<bool>& removed, ExperimentResult& experiment_result) {
    size_t plan_size = plan.get_length();
    SAS_State checking_state = s;
    std::vector<size_t> marks;
    int cost = plan.get_operator(k).get_cost();
    marks.push_back(k);
    for (size_t i = k + 1; i < plan_size; i += 1) {
        if (!removed[i]) {
            if (!checking_state.check_and_apply(plan.get_operator(i))) {
                if (is_landmark[i]) {
                    experiment_result.increase_pals_used();
                    break;
                } else {
                    marks.push_back(i);
                    cost += plan.get_operator(i).get_cost();
                }
            }
        }
    }
    if (checking_state.is_goal()) {
        experiment_result.increase_sets_detected();
        experiment_result.append_set_lenght(marks.size());
        experiment_result.append_set_span(marks[marks.size() - 1] - marks[0]);
        return std::make_pair(cost, marks);
    } else { 
        return std::make_pair(-1, std::vector<size_t>());
    }
}