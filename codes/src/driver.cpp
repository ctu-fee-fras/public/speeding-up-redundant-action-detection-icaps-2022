#include "driver.hpp"

#include <string>

int PlannerProxy::run(const std::string domain, const std::string problem, const std::string target, bool verbose) {

    size_t len = domain.size() + problem.size() + command.size() + 50;
    if (len > 10000) {
        std::cerr << "Command is too long." << std::endl;
        exit(1);
    }

    std::string pr_command = "prlimit --cpu=300 --as=8000000000 " + command + " > /dev/null";
    //pr_command = "prlimit --cpu=300 --as=8000000000 " + command;

    char c[10000];
    sprintf(c, pr_command.c_str(), domain.c_str(), problem.c_str(), target.c_str());


    if (verbose) std::cout << "Planning with planner " << name << " started." << std::endl;
    int r = std::system(c);
    if (verbose) std::cout << "Planning with planner " << name << " ended." << std::endl;

    if (std::filesystem::exists(target)) {
        return WEXITSTATUS(r);
    } else {
        return -1;
    }


}

std::pair<int, std::chrono::duration<long, std::micro>> PlannerProxy::timed_run(const std::string domain, const std::string problem, const std::string target, bool verbose) {

    auto start = std::chrono::high_resolution_clock::now();
    int return_code = run(domain, problem, target, verbose);
    auto end = std::chrono::high_resolution_clock::now();
    
    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::pair<int, std::chrono::duration<long, std::micro>> pair = std::make_pair(return_code, microseconds);

    return pair;
}
