#include "gaecfd.hpp"

SAS_Plan GAECfd::optimize(SAS_Plan plan, ExperimentResult& experiment_result) {
    size_t plan_size = plan.get_length();
    std::vector<bool> removed(plan_size, false);
    int best_index;
    std::tuple<int, bool, std::vector<size_t>> best;
    do {
        best_index = -1;
        best = std::tuple(0, false, std::vector<size_t>());
        SAS_State s = plan.get_problem()->get_init();
        for (size_t i = 0; i < plan_size; i += 1) {
            if (!removed[i]) {
                const auto& result = evaluate_remove(plan, i, s, removed, experiment_result);
                if (std::get<0>(result) >= std::get<0>(best)) {
                    best_index = (int)i;
                    best = result;
                }
                s.apply(plan.get_operator(i));
            }
        }
        if (best_index != -1) {
            for (auto index : std::get<2>(best)) {
                removed[index] = true;
            }
        }
    } while (best_index != -1);
    for (size_t i = 0; i < plan_size; i += 1) {
        if (removed[plan_size - 1 - i]) {
            plan.remove(plan_size - 1 - i);
        }
    }
    return plan;
}
std::tuple<int, bool, std::vector<size_t>> GAECfd::evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& s, const std::vector<bool>& removed, ExperimentResult& experiment_result) {
    size_t plan_size = plan.get_length();
    SAS_State checking_state = s;
    SAS_State original_state = s;
    original_state.apply(plan.get_operator(k));
    bool cycle_detected = false;
    int cost = plan.get_operator(k).get_cost();
    std::vector<size_t> marks;
    marks.push_back(k);
    for (size_t i = k + 1; i < plan_size; i += 1) {
        if (!removed[i]) {
            original_state.apply(plan.get_operator(i));
            if (!checking_state.check_and_apply(plan.get_operator(i))) {
                cost += plan.get_operator(i).get_cost();
                marks.push_back(i);
            }
            cycle_detected = original_state == checking_state;
            if (cycle_detected) {
                experiment_result.increase_cycles_used();
                break;
            }
        }
    }
    if (cycle_detected || checking_state.is_goal()) {
        experiment_result.increase_sets_detected();
        experiment_result.append_set_lenght(marks.size());
        experiment_result.append_set_span(marks[marks.size() - 1] - marks[0]);
        return std::make_tuple(cost, cycle_detected, marks);
    } else { 
        return std::make_tuple(-1, false, std::vector<size_t>());
    }
}