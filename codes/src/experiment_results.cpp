#include "experiment_results.hpp"

ExperimentResult::ExperimentResult(const std::string _planner_name, const std::string _optimizer_name, const std::string _benchmark_name, const std::string _benchmark_group, SAS_Plan& plan, std::chrono::duration<long, std::micro> _planning_consumed)
    : planner_name(_planner_name), optimizer_name(_optimizer_name), benchmark_name(_benchmark_name), benchmark_group(_benchmark_group), plan_cost(plan.get_cost()), plan_lenght(plan.get_length()), time_planning_consumed(_planning_consumed) {}

void ExperimentResult::append_to_csv(std::string filepath, std::string delimiter) {

    std::ofstream file;
    file.open(filepath, std::ios_base::app);

    if (!file.is_open()) {
        std::cerr << "No data appended. File " + filepath + " couldn't be opened." << std::endl;
        return;
    }

    file << serialize(delimiter);
}

std::string ExperimentResult::serialization_header(std::string delimiter) {
    std::string serialization = "";
    serialization += "planner_name" + delimiter;
    serialization += "optimizer_name" + delimiter;
    serialization += "benchmark_name" + delimiter;
    serialization += "benchmark_group" + delimiter;
    serialization += "plan_cost" + delimiter;
    serialization += "optimized_cost" + delimiter;
    serialization += "plan_lenght" + delimiter;
    serialization += "optimized_lenght" + delimiter;
    serialization += "time_planning_consumed" + delimiter;
    serialization += "time_optimizing_consumed" + delimiter;
    serialization += "sets_detected" + delimiter;
    serialization += "mean_of_sets_lenght" + delimiter;
    serialization += "mean_of_sets_span" + delimiter;
    serialization += "pals_found" + delimiter;
    serialization += "pals_used" + delimiter;
    serialization += "landmark_search_consumed" + delimiter;
    serialization += "cycles_used" + delimiter;
    serialization += "landmarks_sequence_lenght" + delimiter;
    serialization += "landmarks_found_sequence\n";
    return serialization;
}

std::string ExperimentResult::serialize(std::string delimiter) {
    std::string serialization = "";
    serialization += planner_name + delimiter;
    serialization += optimizer_name + delimiter;
    serialization += benchmark_name + delimiter;
    serialization += benchmark_group + delimiter;
    serialization += std::to_string(plan_cost) + delimiter;
    serialization += std::to_string(optimized_cost) + delimiter;
    serialization += std::to_string(plan_lenght) + delimiter;
    serialization += std::to_string(optimized_lenght) + delimiter;
    serialization += std::to_string(time_planning_consumed.count()) + delimiter;
    serialization += std::to_string(time_optimizing_consumed.count()) + delimiter;
    serialization += std::to_string(sets_detected) + delimiter;
    serialization += sets_lenght.size() == 0 ? "0" + delimiter : std::to_string((double) std::accumulate(sets_lenght.begin(), sets_lenght.end(), 0) /  (double) sets_lenght.size()) + delimiter;
    serialization += sets_span.size() == 0 ? "0" + delimiter : std::to_string((double) std::accumulate(sets_span.begin(), sets_span.end(), 0) /  (double) sets_span.size()) + delimiter;
    serialization += std::to_string(pals_found) + delimiter;
    serialization += std::to_string(pals_used) + delimiter;
    serialization += std::to_string(time_optimizing_consumed.count()) + delimiter;
    serialization += std::to_string(cycles_used) + delimiter;
    serialization += std::to_string(landmarks_sequence_lenght) + delimiter;
    serialization += landmarks_found_string + '\n';
    return serialization;
}

void ExperimentResult::append_landmarks_found(unsigned int landmarks_count) { 
    if (landmarks_found_string == "") { 
        landmarks_found_string += std::to_string(landmarks_count);
    } else {
        landmarks_found_string += "," + std::to_string(landmarks_count);
    }
}

void ExperimentResults::dump_all() {
    for (auto &result : results) {
        std::cout << result.serialize() << std::endl;
    }
}

void ExperimentResults::to_csv(std::string filepath, std::string delimiter) {

    std::ofstream file;
    file.open(filepath, std::ios_base::out);

    if (!file.is_open()) {
        std::cerr << "File " + filepath + " couldn't be opened. No data has been written." << std::endl;
        return;
    }

    file << ExperimentResult::serialization_header(delimiter);
    for (auto &result : results) {
        file << result.serialize(delimiter);
    }
}

void ExperimentResults::add(ExperimentResult result) { results.emplace_back(result); }
