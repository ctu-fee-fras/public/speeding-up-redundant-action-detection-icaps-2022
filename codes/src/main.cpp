#include <functional>
#include <iostream>
#include <string>

#include "SAS_parser.hpp"
#include "SAS_plan.hpp"
#include "SAS_problem.hpp"
#include "ae.hpp"
#include "aecl.hpp"
#include "aec.hpp"
#include "aecfd.hpp"
#include "ael.hpp"
#include "benchmark.hpp"
#include "driver.hpp"
#include "experiment.hpp"
#include "gae.hpp"
#include "gaecl.hpp"
#include "gaec.hpp"
#include "gaecfd.hpp"
#include "gael.hpp"
#include "optimizer.hpp"

std::vector<Benchmark> get_all_benchmarks();
std::vector<Benchmark> get_settlers_benchmarks();
std::vector<Benchmark> get_termes_benchmarks();
std::vector<Benchmark> get_elevator_benchmarks();
std::vector<Benchmark> get_tetris_benchmarks();
std::vector<Benchmark> get_barman_benchmarks();
std::vector<Benchmark> get_hiking_benchmarks();
std::vector<Benchmark> get_nurikabe_benchmarks();
std::vector<Benchmark> get_spider_benchmarks();
std::vector<Benchmark> get_snake_benchmarks();
std::vector<Benchmark> get_agricola_benchmarks();
std::vector<Benchmark> get_caldera_benchmarks();
std::vector<Benchmark> get_caldera_split_benchmarks();
std::vector<Benchmark> get_data_network_benchmarks();
std::vector<Benchmark> get_flashfill_benchmarks();
std::vector<Benchmark> get_organic_synthesis_benchmarks();
std::vector<Benchmark> get_organic_synthesis_split_benchmarks();
std::vector<Benchmark> get_termes_benchmarks();

std::vector<Benchmark> get_ipc2018_benchmarks();
std::vector<Benchmark> get_ipc2014_benchmarks();
std::vector<Benchmark> get_ipc2014_benchmark(const std::string &name, size_t start = 1, size_t end = 20);

std::vector<PlannerProxy> get_planners();

int main(int argc, char *argv[]) {

    if (argc != 2) {
        std::cerr << "Invalid number of arguments. Only path to output .csv file is required." << std::endl;
        exit(1);
    }

    AE ae("AE");
    AEL ael("AEL");
    AEC aec("AEC");
    // AECfd aecfd("AECfd");
    AECL aecl("AECL");
    GAE gae("GAE");
    GAEL gael("GAEL");
    GAEC gaec("GAEC");
    // GAECfd gaecfd("GAECfd");
    GAECL gaecl("GAECL");

    auto benchmarks = get_ipc2018_benchmarks();
    auto ipc2014 = get_ipc2014_benchmarks();
    benchmarks.insert(benchmarks.end(), ipc2014.begin(), ipc2014.end());

    auto planners = get_planners();
    
    std::vector<std::reference_wrapper<Optimizer>> optimizers;
    optimizers.push_back(ae);
    optimizers.push_back(ael);
    optimizers.push_back(aec);
    // optimizers.push_back(aecfd);
    optimizers.push_back(aecl);
    optimizers.push_back(gae);
    optimizers.push_back(gael);
    optimizers.push_back(gaec);
    // optimizers.push_back(gaecfd);
    optimizers.push_back(gaecl);

    Experiment e(benchmarks, planners, optimizers);
    e.save_while_running(argv[1]);
    e.run();

    return 0;
}

std::vector<PlannerProxy> get_planners() {
    std::vector<PlannerProxy> planners{
        PlannerProxy("lama-first", LAMA_FIRST_SETTING),
        PlannerProxy("LAPKT-BFWS-Preference", LAPKT_BFWS_PREFERENCE_SETTING),
        PlannerProxy("freelunch-madagascar", FREELUNCH_MADAGASCAR_SETTING),
        PlannerProxy("cerberus", CERBERUS_SETTING),
        PlannerProxy("yahsp", YAHSP_SETTING),
    };
    return planners;
}

std::vector<Benchmark> get_termes_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/termes/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("termes" + leading_zero + std::to_string(i), "termes",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_nurikabe_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/nurikabe/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("nurikabe" + leading_zero + std::to_string(i), "nurikabe",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_snake_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/snake/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("snake" + leading_zero + std::to_string(i), "snake",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_spider_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/spider/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("spider" + leading_zero + std::to_string(i), "spider",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_settlers_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/settlers/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("settlers" + leading_zero + std::to_string(i), "settlers",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_hiking_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2014/seq-agl/Hiking/";
    for (size_t i = 1; i <= 20; i += 1) {
        benchmarks.emplace_back(Benchmark(
            "hiking" + std::to_string(i), "hiking", path + "domain.pddl",
            path + "p" + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_barman_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2014/seq-agl/Barman/";
    for (size_t i = 1; i <= 20; i += 1) {
        benchmarks.emplace_back(Benchmark(
            "barman" + std::to_string(i), "barman", path + "domain.pddl",
            path + "p" + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_tetris_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2014/seq-agl/Tetris/";
    for (size_t i = 20; i <= 40; i += 1) {
        if (i == 30) {
            continue;
        }
        benchmarks.emplace_back(Benchmark(
            "tetris" + std::to_string(i), "tetris", path + "domain.pddl",
            path + "p0" + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_elevator_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2008/seq-sat/elevators-strips/";
    for (size_t i = 1; i <= 30; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(Benchmark(
            "elevator" + leading_zero + std::to_string(i), "elevators",
            path + "p" + leading_zero + std::to_string(i) + "-domain.pddl",
            path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_agricola_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/agricola/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("agricola" + leading_zero + std::to_string(i), "agricola",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_caldera_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/caldera/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("caldera" + leading_zero + std::to_string(i), "caldera",
                      path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_caldera_split_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/caldera-split/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("calderasplit" + leading_zero + std::to_string(i),
                      "calderasplit", path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_data_network_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/data-network/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(
            Benchmark("datanetwork" + leading_zero + std::to_string(i),
                      "datanetwork", path + "domain.pddl",
                      path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_flashfill_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/flashfill/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(Benchmark(
            "flashfill" + leading_zero + std::to_string(i), "flashfill",
            path + "domain-" + "p" + leading_zero + std::to_string(i) + ".pddl",
            path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_organic_synthesis_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/organic-synthesis/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(Benchmark(
            "organicsynthesis" + leading_zero + std::to_string(i),
            "organicsynthesis",
            path + "domain-" + "p" + leading_zero + std::to_string(i) + ".pddl",
            path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_organic_synthesis_split_benchmarks() {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2018/sat/organic-synthesis-split/";
    for (size_t i = 1; i <= 20; i += 1) {
        std::string leading_zero = ((i < 10) ? std::string("0") : "");
        benchmarks.emplace_back(Benchmark(
            "organicsynthesissplit" + leading_zero + std::to_string(i),
            "organicsynthesissplit",
            path + "domain-" + "p" + leading_zero + std::to_string(i) + ".pddl",
            path + "p" + leading_zero + std::to_string(i) + ".pddl"));
    }
    return benchmarks;
}

std::vector<Benchmark> get_all_benchmarks() {

    std::vector<Benchmark> benchmarks = get_ipc2014_benchmarks();

    auto elevators = get_elevator_benchmarks();
    benchmarks.insert(benchmarks.end(), elevators.begin(), elevators.end());

    auto settlers = get_settlers_benchmarks();
    benchmarks.insert(benchmarks.end(), settlers.begin(), settlers.end());

    auto termes = get_termes_benchmarks();
    benchmarks.insert(benchmarks.end(), termes.begin(), termes.end());

    auto spider = get_spider_benchmarks();
    benchmarks.insert(benchmarks.end(), spider.begin(), spider.end());

    auto snake = get_snake_benchmarks();
    benchmarks.insert(benchmarks.end(), snake.begin(), snake.end());

    auto agricola = get_agricola_benchmarks();
    benchmarks.insert(benchmarks.end(), agricola.begin(), agricola.end());

    auto caldera = get_caldera_benchmarks();
    benchmarks.insert(benchmarks.end(), caldera.begin(), caldera.end());

    auto caldera_split = get_caldera_split_benchmarks();
    benchmarks.insert(benchmarks.end(), caldera_split.begin(),
                      caldera_split.end());

    auto data_network = get_data_network_benchmarks();
    benchmarks.insert(benchmarks.end(), data_network.begin(),
                      data_network.end());

    auto flashfill = get_flashfill_benchmarks();
    benchmarks.insert(benchmarks.end(), flashfill.begin(), flashfill.end());

    auto organic_synthesis = get_organic_synthesis_benchmarks();
    benchmarks.insert(benchmarks.end(), organic_synthesis.begin(),
                      organic_synthesis.end());

    auto organic_synthesis_split = get_organic_synthesis_split_benchmarks();
    benchmarks.insert(benchmarks.end(), organic_synthesis_split.begin(),
                      organic_synthesis_split.end());

    auto nurikabe = get_nurikabe_benchmarks();
    benchmarks.insert(benchmarks.end(), nurikabe.begin(), nurikabe.end());

    return benchmarks;
}

std::vector<Benchmark> get_ipc2014_benchmark(const std::string &name, size_t start, size_t end) {
    std::vector<Benchmark> benchmarks;
    std::string path = "../domains/ipc2014/seq-agl/" + name + "/";
    if (name == "openstacks") {
        for (size_t i = start; i <= end; i += 1) {
            std::string leading_zero = ((i < 10) ? std::string("0") : "");
            benchmarks.emplace_back(Benchmark(
                name + leading_zero + std::to_string(i), name,
                path + "domain" + leading_zero + std::to_string(i) + ".pddl",
                path + "p" + leading_zero + std::to_string(i) + ".pddl"));
        }
    } else {
        for (size_t i = start; i <= end; i += 1) {
            std::string leading_zero = ((i < 10) ? std::string("0") : "");
            benchmarks.emplace_back(Benchmark(
                name + leading_zero + std::to_string(i), name,
                path + "domain.pddl",
                path + "p" + leading_zero + std::to_string(i) + ".pddl"));
        }
    }
    return benchmarks;
}

std::vector<Benchmark> get_ipc2014_benchmarks() {
    std::vector<Benchmark> benchmarks;
    for (const std::string &name :
         {"barman", "cavediving", "childsnack", "citycar", "floortile", "ged",
          "hiking", "maintenance", "openstacks", "parking", "tetris",
          "thoughtful", "transport", "visitall"}) {
        std::vector<Benchmark> certain = get_ipc2014_benchmark(name);
        benchmarks.insert(benchmarks.end(), certain.begin(), certain.end());
    }
    return benchmarks;
}

std::vector<Benchmark> get_ipc2018_benchmarks() {
    
    std::vector<Benchmark> benchmarks;
    
    auto settlers = get_settlers_benchmarks();
    benchmarks.insert(benchmarks.end(), settlers.begin(), settlers.end());

    auto termes = get_termes_benchmarks();
    benchmarks.insert(benchmarks.end(), termes.begin(), termes.end());

    auto spider = get_spider_benchmarks();
    benchmarks.insert(benchmarks.end(), spider.begin(), spider.end());

    auto snake = get_snake_benchmarks();
    benchmarks.insert(benchmarks.end(), snake.begin(), snake.end());

    auto agricola = get_agricola_benchmarks();
    benchmarks.insert(benchmarks.end(), agricola.begin(), agricola.end());

    auto caldera = get_caldera_benchmarks();
    benchmarks.insert(benchmarks.end(), caldera.begin(), caldera.end());

    auto caldera_split = get_caldera_split_benchmarks();
    benchmarks.insert(benchmarks.end(), caldera_split.begin(),
                      caldera_split.end());

    auto data_network = get_data_network_benchmarks();
    benchmarks.insert(benchmarks.end(), data_network.begin(),
                      data_network.end());

    auto flashfill = get_flashfill_benchmarks();
    benchmarks.insert(benchmarks.end(), flashfill.begin(), flashfill.end());

    auto organic_synthesis = get_organic_synthesis_benchmarks();
    benchmarks.insert(benchmarks.end(), organic_synthesis.begin(),
                      organic_synthesis.end());

    auto organic_synthesis_split = get_organic_synthesis_split_benchmarks();
    benchmarks.insert(benchmarks.end(), organic_synthesis_split.begin(),
                      organic_synthesis_split.end());

    auto nurikabe = get_nurikabe_benchmarks();
    benchmarks.insert(benchmarks.end(), nurikabe.begin(), nurikabe.end());

    return benchmarks;
}
