#ifndef ___SAS_PROBLEM___
#define ___SAS_PROBLEM___

#include <functional>
#include <vector>
#include <string>

class SAS_State;

#include "SAS_state.hpp"
#include "SAS_variable_info.hpp"
#include "SAS_mutex.hpp"
#include "SAS_operator.hpp"
#include "SAS_axiom.hpp"
#include "SAS_exit_codes.hpp"

class SAS_Problem {

    public:
        
        SAS_Problem(unsigned int version, bool metric, std::vector<SAS_Variable_Info> variables, std::vector<SAS_Mutex> mutexes, SAS_State init, SAS_State goal, std::vector<SAS_Operator> operators, std::vector<SAS_Axiom> axioms);

        const std::vector<SAS_Axiom>& get_axioms() const { return _axioms; }
        const SAS_State& get_init() const { return _init; }
        const SAS_State& get_goal() const { return _goal; }
        inline std::vector<SAS_Variable_Info>& get_variables() { return _variables; }
        std::vector<std::vector<std::reference_wrapper<SAS_Axiom>>>& get_grouped_axioms();
        SAS_Operator& find_operator_by_name(std::string name);

    private:
        unsigned int _version;
        bool _metric;
        std::vector<SAS_Variable_Info> _variables;
        std::vector<SAS_Mutex> _mutexes;
        SAS_State _init;
        SAS_State _goal;
        std::vector<SAS_Operator> _operators;
        std::vector<SAS_Axiom> _axioms;
        std::vector<std::vector<std::reference_wrapper<SAS_Axiom>>> _grouped_axioms;

        void link_problem_to_states();  
        void group_axioms();                    // group axioms to the layers according to the variables
};

#endif