#ifndef ___SAS_OPERATOR___
#define ___SAS_OPERATOR___

#include <utility>
#include <vector>
#include <string>
#include <functional>

#include "SAS_assignment.hpp"

class SAS_Operator {
    public:
        SAS_Operator(std::string name, SAS_Assignment preconditions, std::vector<std::pair<SAS_Assignment, SAS_Assignment>> effects, unsigned int cost) : _name(name), _preconditions(preconditions), _effects(effects), _cost(cost) {}
        inline const std::vector<std::pair<SAS_Assignment, SAS_Assignment>>& get_effects() const { return _effects; }
        inline const SAS_Assignment& get_preconditions() const { return _preconditions; }
        inline std::string get_name() const { return _name; }
        inline unsigned int get_cost() const { return _cost; }

    protected:
        std::string _name;
        SAS_Assignment _preconditions;
        std::vector<std::pair<SAS_Assignment, SAS_Assignment>> _effects;
        unsigned int _cost;

};

#endif