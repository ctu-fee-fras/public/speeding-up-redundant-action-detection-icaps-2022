#ifndef ___SAS_VARIABLE_INFO___
#define ___SAS_VARIABLE_INFO___

#include <assert.h>
#include <string>
#include <vector>

class SAS_Variable_Info {

    public:
        SAS_Variable_Info(unsigned int id, std::string name, int axiom_layer, unsigned int range, std::vector<std::string> values);
        inline unsigned int get_id() { return _id; }
        inline unsigned int get_range() { return _range; }
        inline int get_axiom_layer() { return _axiom_layer; }

    private:
        unsigned int _id;
        std::string _name;
        int _axiom_layer;
        unsigned int _range;
        std::vector<std::string> _values;

};

#endif