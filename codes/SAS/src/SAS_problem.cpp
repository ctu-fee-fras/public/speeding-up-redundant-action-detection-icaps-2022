#include "SAS_problem.hpp"

SAS_Problem::SAS_Problem(unsigned int version, bool metric, std::vector<SAS_Variable_Info> variables, std::vector<SAS_Mutex> mutexes, SAS_State init, SAS_State goal, std::vector<SAS_Operator> operators, std::vector<SAS_Axiom> axioms) : _version(version), _metric(metric), _variables(variables), _mutexes(mutexes), _init(init), _goal(goal), _operators(operators), _axioms(axioms) {
    link_problem_to_states();
    group_axioms();
}

void SAS_Problem::link_problem_to_states() {
    _init.set_problem(this);
    _goal.set_problem(this);
}

void SAS_Problem::group_axioms() {
    _grouped_axioms.clear();
    for (auto& axiom : _axioms) {
        int layer = _variables[axiom.get_affected_variable()].get_axiom_layer();
        assert(layer >= -1);
        unsigned int index = (unsigned int) (layer + 1);
        while (index >= _grouped_axioms.size()) {
            std::vector<std::reference_wrapper<SAS_Axiom>> v;
            _grouped_axioms.emplace_back(v);
        }
        _grouped_axioms[index].emplace_back(axiom);
    }
}

std::vector<std::vector<std::reference_wrapper<SAS_Axiom>>>& SAS_Problem::get_grouped_axioms() {
    if (_grouped_axioms.empty()) {
        group_axioms();
    }
    return _grouped_axioms;
}

SAS_Operator& SAS_Problem::find_operator_by_name(std::string name) {
    for (auto& o : _operators) {
        if (o.get_name() == name) {
            return o;
        }
    }
    std::cerr << "No operator with name '" << name << "' found. (It could be optimized out by translator.)" << std::endl;
    throw std::string("No operator with name '" + name + "' found. (It could be optimized out by translator.)");
}