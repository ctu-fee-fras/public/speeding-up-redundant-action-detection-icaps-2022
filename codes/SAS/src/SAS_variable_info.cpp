#include "SAS_variable_info.hpp"

SAS_Variable_Info::SAS_Variable_Info(unsigned int id, std::string name, int axiom_layer, unsigned int range, std::vector<std::string> values) : _id(id), _name(name), _axiom_layer(axiom_layer), _range(range), _values(values) {}
