#ifndef ___THESES_EXPERIMENT_RESULTS___
#define ___THESES_EXPERIMENT_RESULTS___

#include "SAS_plan.hpp"

#include <vector>
#include <string>
#include <chrono>
#include <fstream>
#include <iostream>
#include <numeric>

#define EXPERIMENT_DEFAULT_DELIMITER ";"

class ExperimentResult {

    private:

        std::string planner_name;
        std::string optimizer_name; 
        std::string benchmark_name;
        std::string benchmark_group;

        unsigned long plan_cost;
        unsigned long optimized_cost;
        
        unsigned long plan_lenght;
        unsigned long optimized_lenght;

        std::chrono::duration<long, std::micro> time_planning_consumed;
        std::chrono::duration<long, std::micro> time_optimizing_consumed;

        unsigned int pals_found = 0;
        unsigned int pals_used = 0;
        unsigned int cycles_used = 0;
        unsigned int sets_detected = 0;
        unsigned int landmarks_sequence_lenght = 0;
        std::string landmarks_found_string;
        std::chrono::duration<long, std::micro> landmark_search_consumed;
        std::vector<unsigned int> sets_lenght;
        std::vector<unsigned int> sets_span;

    public:

        static std::string serialization_header(std::string delimiter = EXPERIMENT_DEFAULT_DELIMITER);

        ExperimentResult(const std::string planner_name, const std::string optimizer_name, const std::string benchmark_name, const std::string benchmark_group, SAS_Plan& plan, std::chrono::duration<long, std::micro> planning_consumed);

        unsigned long absolute_cost_improvement() { return plan_cost - optimized_cost; }
        unsigned long absolute_lenght_improvement() { return plan_lenght - optimized_lenght; }
        
        float relative_cost_improvement() { return (plan_cost == 0) ? 0.0 : ((float) absolute_cost_improvement()) / ((float) plan_cost); }
        float relative_lenght_improvement() { return (plan_lenght == 0) ? 0.0 : ((float) absolute_lenght_improvement()) / ((float) plan_lenght); }

        const std::string get_planner_name() { return planner_name; }
        const std::string get_optimizer_name() { return optimizer_name; }
        const std::string get_benchmark_name() { return benchmark_name; }
        const std::string get_benchmark_group() { return benchmark_group; }
        unsigned long get_plan_cost() { return plan_cost; }
        unsigned long get_optimized_cost() { return optimized_cost; }
        unsigned long get_plan_lenght() { return plan_lenght; }
        unsigned long get_optimized_lenght() { return optimized_lenght; }
        const std::chrono::duration<long, std::micro> get_time_optimizing_consumed() { return time_optimizing_consumed; }
        unsigned int get_sets_detected() { return sets_detected; }
        unsigned int get_pals_found() { return pals_found; }
        unsigned int get_pals_used() { return pals_used; }
        unsigned int get_cycles_used() { return cycles_used; }
        unsigned int get_landmarks_sequence_lenght() { return landmarks_sequence_lenght; }
        std::string get_landmarks_found_iterative() { return landmarks_found_string; }
        const std::chrono::duration<long, std::micro> get_time_landmark_search_consumed() { return time_optimizing_consumed; }

        void set_optimized_cost(unsigned long c) { optimized_cost = c; };
        void set_optimized_length(unsigned long l) { optimized_lenght = l; };
        void set_time_optimizing_consumed(std::chrono::duration<long, std::micro> t) { time_optimizing_consumed = t; };
        void set_pals_found(unsigned int _pals_found) { pals_found = _pals_found; }
        void set_time_landmark_search_consumed(std::chrono::duration<long, std::micro> t) { landmark_search_consumed = t; }

        void increase_pals_found() { pals_found += 1; }
        void increase_pals_used() { pals_used += 1; }
        void increase_cycles_used() { cycles_used += 1; }
        void increase_sets_detected() { sets_detected += 1; }
        void increase_landmarks_sequence_lenght() { landmarks_sequence_lenght += 1; }
        void append_landmarks_found(unsigned int landmarks_count);
        void append_set_lenght(size_t lenght) { sets_lenght.push_back((unsigned int) lenght); }
        void append_set_span(size_t lenght) { sets_span.push_back((unsigned int) lenght); }

        void append_to_csv(std::string filename, std::string delimiter = EXPERIMENT_DEFAULT_DELIMITER);

        std::string serialize(std::string delimiter = EXPERIMENT_DEFAULT_DELIMITER);

};

class ExperimentResults {

    private:
        std::vector<ExperimentResult> results;       

    public:
        void add(ExperimentResult result);
        void dump_all();
        void to_csv(std::string filepath, std::string delimiter = EXPERIMENT_DEFAULT_DELIMITER);

};

#endif