#ifndef ___THESES_AECfd___
#define ___THESES_AECfd___

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "utils.hpp"

class AECfd : public Optimizer {

    public:
        AECfd(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

};

#endif