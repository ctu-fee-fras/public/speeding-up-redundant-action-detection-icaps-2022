#ifndef ___THESES_DRIVER___
#define ___THESES_DRIVER___

#include <stdlib.h>
#include <string>
#include <iostream>
#include <chrono>
#include <functional>
#include <utility>
#include <filesystem>

// translator
#define FD_TRANSLATOR_SETTING "./planners/downward/fd_switcher.sh %s %s %s"

// agile planners
#define LAMA_FIRST_SETTING "./planners/explicit-baseline-planners/fd_switcher.sh %s %s %s"
#define LAPKT_BFWS_PREFERENCE_SETTING "./planners/LAPKT-BFWS-Preference/BFWS/fd-version/bfws_f5.py %s %s %s"
#define FREELUNCH_MADAGASCAR_SETTING "./planners/freelunch-madagascar/plan.sh %s %s %s"
#define FREELUNCH_DOUBLE_RELAXED_SETTING "./planners/freelunch-double-relaxed/plan.sh %s %s %s"
#define CERBERUS_SETTING "./planners/cerberus/plan.py %s %s %s"
#define YAHSP_SETTING "./planners/cpt-yahsp/run.sh %s %s %s"

// satisfying planners
#define FDSS_2018_SETTING "./planners/fdss2018/fast-downward.py --portfolio \"./planners/fdss2018/driver/portfolios/seq_sat_fdss_2018.py\" --search-time-limit 30m %s %s > /dev/null"

class PlannerProxy {
    
    private:
        const std::string name;
        std::string command;

    public:
        PlannerProxy(const std::string name_string, const char* command_string) : name(name_string), command(std::string(command_string)) {};
        PlannerProxy(const std::string name_string, const std::string command_string) : name(name_string), command(command_string) {};
        const std::string get_name() { return name; }
        const std::string get_command() { return command; }
        int run(const std::string domain, const std::string problem, const std::string target, bool verbose=false);
        std::pair<int, std::chrono::duration<long, std::micro>> timed_run(const std::string domain, const std::string problem, const std::string target, bool verbose=false);

};

#endif
