#ifndef ___THESES_GAECfd___
#define ___THESES_GAECfd___

#include <vector>
#include <functional>

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "utils.hpp"

class GAECfd : public Optimizer {

    public:
        GAECfd(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& experiment_result);

    private:
        std::tuple<int, bool, std::vector<size_t>> evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& state, const std::vector<bool>& removed, ExperimentResult& experiment_result);

};

#endif
