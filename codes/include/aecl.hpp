#ifndef ___THESES_AECL___
#define ___THESES_AECL___

#include "SAS_plan.hpp"
#include "optimizer.hpp"
#include "landmarks.hpp"
#include "utils.hpp"

class AECL : public Optimizer {

    public:
        AECL(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

};

#endif