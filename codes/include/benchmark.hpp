#ifndef ___THESES_BENCHMARK___
#define ___THESES_BENCHMARK___

#include <string>
#include <iostream>
#include <vector>

#include "driver.hpp"
#include "optimizer.hpp"
#include "benchmark.hpp"

class Benchmark {
    
    private:
        std::string name;
        std::string group;
        std::string domain_path;
        std::string problem_path;

    public:
        Benchmark(std::string _name, std::string _group, std::string _domain_path, std::string _problem_path) : name(_name), group(_group), domain_path(_domain_path), problem_path(_problem_path) {}
        const std::string get_name() { return name; }
        const std::string get_group() { return  group; }
        const std::string get_domain() { return  domain_path; }
        const std::string get_problem() { return  problem_path; }

};

#endif
