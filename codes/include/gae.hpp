#ifndef ___THESES_GAE___
#define ___THESES_GAE___

#include <vector>
#include <functional>
#include <utility>
#include <memory>

#include "SAS_plan.hpp"
#include "optimizer.hpp"

class GAE : public Optimizer {

    public:
        GAE(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

    private:
        std::pair<int, std::vector<size_t>> evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& s, const std::vector<bool>& removed, ExperimentResult& result);

};

#endif