#ifndef ___THESES_AELSE___
#define ___THESES_AELSE___

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "landmarks.hpp"

class AEL : public Optimizer {

    public:
        AEL(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

};

#endif