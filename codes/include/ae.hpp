#ifndef ___THESES_AE___
#define ___THESES_AE___

#include "SAS_plan.hpp"
#include "optimizer.hpp"

class AE : public Optimizer {

    public:
        AE(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

};

#endif