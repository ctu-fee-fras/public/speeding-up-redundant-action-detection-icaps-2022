#ifndef ___THESES_GAELSe___
#define ___THESES_GAELSe___

#include <vector>
#include <functional>
#include <utility>
#include <memory>

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "landmarks.hpp"

class GAEL : public Optimizer {

    public:
        GAEL(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

    private: 
        std::pair<int, std::vector<size_t>> evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& s, const std::vector<bool>& is_landmark, const std::vector<bool>& removed, ExperimentResult& result);

};

#endif