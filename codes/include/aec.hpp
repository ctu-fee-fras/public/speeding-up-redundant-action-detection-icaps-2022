#ifndef ___THESES_AEC___
#define ___THESES_AEC___

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "utils.hpp"

class AEC : public Optimizer {

    public:
        AEC(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

};

#endif