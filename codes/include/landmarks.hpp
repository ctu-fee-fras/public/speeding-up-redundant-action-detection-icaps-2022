#ifndef ___THESES_LANDMARKS___
#define ___THESES_LANDMARKS___

#include <vector>
#include <set>
#include <map>
#include <utility>
#include <memory>
#include <numeric>

#include "SAS_operator.hpp"
#include "SAS_plan.hpp"
#include "SAS_state.hpp"

#include "experiment_results.hpp"

std::vector<bool> find_landmarks(const SAS_Plan& plan, bool enhance, ExperimentResult& experiment_result);
std::vector<bool> find_landmarks_enhanced(const SAS_Plan& plan, ExperimentResult& experiment_result);
std::vector<bool> iterative_landmark_search(const SAS_Plan& plan, ExperimentResult& experiment_result);
std::set<std::pair<unsigned int, unsigned int>> get_maximal_effects(const SAS_Operator& o);
std::set<std::pair<unsigned int, unsigned int>> get_minimal_effects(const SAS_Operator& o);



#endif