#ifndef ___THESES_UTILS___
#define ___THESES_UTILS___

#include "SAS_plan.hpp"
#include "SAS_assignment.hpp"
#include "SAS_state.hpp"
#include "SAS_operator.hpp"

SAS_Assignment get_grounded_effects(const SAS_Operator& o, const SAS_State& state);
void update_states_along_plan(std::vector<SAS_State>& states, const SAS_Plan& plan, const std::vector<bool>& removed, size_t start_with_action_index, size_t end_with_action_index);
std::vector<SAS_State> get_states_along_plan(const SAS_Plan& plan, const std::vector<bool>& removed);


#endif