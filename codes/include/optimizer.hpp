#ifndef ___THESES_OPTIMIZER___
#define ___THESES_OPTIMIZER___

#include <chrono>
#include <utility>

#include "SAS_plan.hpp"
#include "experiment_results.hpp"
class Optimizer {

    private:
        const std::string name;

    public:
        Optimizer(std::string _name) : name(_name) {};
        const std::string get_name() const { return name; }

        virtual SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result) = 0;
        std::pair<SAS_Plan, std::chrono::duration<long, std::micro>> timed_optimization(SAS_Plan& plan, ExperimentResult& result, bool verbose = false);
};

#endif