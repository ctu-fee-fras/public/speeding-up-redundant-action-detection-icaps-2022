#ifndef ___THESES_GAEC___
#define ___THESES_GAEC___

#include <vector>
#include <functional>

#include "SAS_plan.hpp"

#include "optimizer.hpp"
#include "utils.hpp"

class GAEC : public Optimizer {

    public:
        GAEC(std::string _name) : Optimizer(_name) {};
        SAS_Plan optimize(SAS_Plan plan, ExperimentResult& result);

    private: 
        std::pair<int, std::vector<size_t>> evaluate_remove(SAS_Plan& plan, size_t k, const SAS_State& s, const std::vector<bool>& removed, ExperimentResult& result);

};

#endif