#ifndef ___THESES_EXPERIMENT___
#define ___THESES_EXPERIMENT___

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <functional>
#include <filesystem>

#include "SAS_parser.hpp"
#include "driver.hpp"
#include "optimizer.hpp"
#include "benchmark.hpp"
#include "experiment_results.hpp"

#define PLANS_LOCATION "../plans/"

class Experiment {
    
    private:

        bool finished = false;
        ExperimentResults results;

        std::vector<Benchmark> benchmarks;
        std::vector<PlannerProxy> planners;
        std::vector<std::reference_wrapper<Optimizer>> optimizers;

        bool save_while_running_enabled;
        std::string save_while_running_filepath;
        std::string save_while_running_delimiter;

    public:
        Experiment(std::vector<Benchmark> _benchmarks, std::vector<PlannerProxy> _planners, std::vector<std::reference_wrapper<Optimizer>> _optimizers);
        void run(bool verbose=true);
        void display();
        void save_while_running(std::string csv_filepath, std::string delimiter = ";");
        void to_csv(std::string filename, std::string delimiter = ";");
        void restart();
        static std::chrono::microseconds generate_plan(PlannerProxy& planner, Benchmark& benchmark, const std::filesystem::path plan_filepath, const std::filesystem::path time_filepath);
        static std::filesystem::path get_plan_filepath(PlannerProxy& planner, Benchmark& benchmark);
        

};

#endif
