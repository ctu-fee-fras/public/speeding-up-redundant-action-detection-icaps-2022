# Speeding up Redundant Action Detection (ICAPS 2022)

## About

This repository contains code base used for experiments of paper authored by *Jakub Med* and *Lukas Chrpa* publised at *ICAPS 2022* titled *On Speeding Up Methods for Identifying Redundant Actions in Plans*.

### Author contacts

* Jakub Med, medjaku1@fel.cvut.cz
* Lukas Chrpa, chrpaluk@fel.cvut.cz

***

## How to run

### Compilation

All codes are placed in the `./codes/` folder. This code can be `make` command used on `./codes/Makefile` file and we recommend to run `make` from `./codes/` folder. 

### Requirements

* `make`
* `g++` (developed and run with `g++-8`, but any version which supports `-std=c++17` should work)

### Evaluation

To perform experiments run `./main <target_csv_file>`. Experiment results will be written to `<target_csv_file>` simultaniously.

Number of experiments, selected optimizers and more can be easily changed in the `./codes/src/main.cpp` file.

***

## Domains & Results

In these folders original IPC domains and plans found by planners are stored.

## Results

As mentioned, data are saved into csv file. Meaning of each column is as follows:

* `planner_name` - self-explaining
* `optimizer_name` - self-explaining
* `benchmark_name` - self-explaining
* `benchmark_group` - self-explaining
* `plan_cost` - self-explaining
* `optimized_cost` - cost of the plan after optimization
* `plan_lenght` -  self-explaining
* `optimized_lenght` - lenght of the plan after optimization
* `time_planning_consumed` - time which planning consumed
* `time_optimizing_consumed` - time which optimization method consumed
* `sets_detected` - number of sets of redundant actions
* `mean_of_sets_lenght` - mean of sets cardinalities
* `mean_of_sets_span` - mean of distances between the first and the last action in the set of redundant actions
* `pals_found` - number of PALs found
* `pals_used` - how many times PALs helped with optimization
* `landmark_search_consumed` - time which landmark search consumed
* `cycles_used` - how many times AC helped with optimization
* `landmarks_sequence_lenght` - not used in this study (legacy code)
* `landmarks_found_sequence` - not used in this study (legacy code)

### How to display results

The data can be automatically displayed using `python3`, prepared `jupyter` notebook and `pandas` library. The notebook is stored in the `./results/` folder and is named `visualizer.ipynb`. Once run, the table, similar to the one in the paper, will be rendered at the end of the notebook. Input `csv` file, separator character, optimizers and columns to display are specified in the notebook and can be easily modified. 

#### Notebook Requirements

* `python3` (tested with version `3.9.7`)
* `pandas` (tested with version `1.4.1`)
* `numpy` (tested with version `1.22.3`)
* `jupyter` notebook support

***

## What is missing

We used five planners, whose code is not included. Class connecting planners with our binary is named `PlannerProxy` and is defined in `./codes/include/driver.hpp`. If you wish to bind own planners to this tool, feel free to modify it.
